/*
 * Copyright (C) 2020. Entgra (Pvt) Ltd, https://entgra.io
 * All Rights Reserved.
 *
 * Unauthorized copying/redistribution of this file, via any medium
 * is strictly prohibited.
 * Proprietary and confidential.
 *
 * Licensed under the Entgra Commercial License,
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * You may obtain a copy of the License at
 * https://entgra.io/licenses/entgra-commercial/1.0
 */

import { message, notification } from 'antd';
import config from '../../../public/conf/config.json';

export const handleApiError = (
  error,
  errorMessage,
  isForbiddenMessageSilent = false,
) => {
  if (
    Object.prototype.hasOwnProperty.call(error, 'response') &&
    error.response.status === 401
  ) {
    message.error('You are not logged in');
    const redirectUrl = encodeURI(window.location.href);
    window.location.href =
      window.location.origin +
      `/${config.appName}/login?redirect=${redirectUrl}`;
    // silence 403 forbidden message
  } else if (
    !(
      isForbiddenMessageSilent &&
      Object.prototype.hasOwnProperty.call(error, 'response') &&
      error.response.status === 403
    )
  ) {
    notification.error({
      message: 'There was a problem',
      duration: 10,
      description: errorMessage,
    });
  }
};
