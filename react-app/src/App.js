/*
 * Copyright (C) 2020. Entgra (Pvt) Ltd, https://entgra.io
 * All Rights Reserved.
 *
 * Unauthorized copying/redistribution of this file, via any medium
 * is strictly prohibited.
 * Proprietary and confidential.
 *
 * Licensed under the Entgra Commercial License,
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * You may obtain a copy of the License at
 * https://entgra.io/licenses/entgra-commercial/1.0
 */

import React from 'react';
import 'antd/dist/antd.less';
import RouteWithSubRoutes from './components/RouteWithSubRoutes';
import { BrowserRouter as Router, Redirect, Switch } from 'react-router-dom';
import axios from 'axios';
import { Layout, Spin, Result } from 'antd';
import ConfigContext from './components/ConfigContext';

const { Content } = Layout;
const loadingView = (
  <Layout>
    <Content
      style={{
        padding: '0 0',
        paddingTop: 300,
        backgroundColor: '#fff',
        textAlign: 'center',
      }}
    >
      <Spin tip="Loading..." />
    </Content>
  </Layout>
);

const errorView = (
  <Result
    style={{
      paddingTop: 200,
    }}
    status="500"
    title="Error occurred while loading the configuration"
    subTitle="Please refresh your browser window"
  />
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      config: {},
    };
  }

  componentDidMount() {
    axios
      .get(
        window.location.origin +
          '/' +
          window.location.pathname.split('/')[1] +
          '/public/conf/config.json',
      )
      .then((res) => {
        const config = res.data;
        this.checkUserLoggedIn(config);
      })
      .catch((error) => {
        this.setState({
          loading: false,
          error: true,
        });
      });
  }

  checkUserLoggedIn = (config) => {
    axios
      .post(window.location.origin + '/entgra-ui-request-handler/user')
      .then((res) => {
        config.user = res.data.data;
        const pageURL = window.location.pathname;
        const lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
        console.log('Last URL segment' + lastURLSegment);
        console.log(config.appName);
        if (lastURLSegment === 'login') {
          window.location.href = window.location.origin + `/${config.appName}/`;
        } else {
          this.setState({
            config: config,
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (
          Object.prototype.hasOwnProperty.call(error, 'response') &&
          error.response.status === 401
        ) {
          const redirectUrl = encodeURI(window.location.href);
          const pageURL = window.location.pathname;
          const lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
          if (lastURLSegment !== 'login') {
            window.location.href =
              window.location.origin +
              `/${config.appName}/login?redirect=${redirectUrl}`;
          } else {
            this.setState({
              loading: false,
              config: config,
            });
          }
        } else {
          this.setState({
            loading: false,
            error: true,
          });
        }
      });
  };

  render() {
    const { loading, error } = this.state;
    const applicationView = (
      <Router>
        <ConfigContext.Provider value={this.state.config}>
          <div>
            <Switch>
              <Redirect
                exact
                from={`/${this.state.config.appName}`}
                to={`/${this.state.config.appName}/sample`}
              />
              {this.props.routes.map((route) => (
                <RouteWithSubRoutes key={route.path} {...route} />
              ))}
            </Switch>
          </div>
        </ConfigContext.Provider>
      </Router>
    );

    return (
      <div>
        {loading && loadingView}
        {!loading && !error && applicationView}
        {error && errorView}
      </div>
    );
  }
}

export default App;
